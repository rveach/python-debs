#! /bin/bash

set -e

# download, verify, extract
curl https://www.python.org/ftp/python/${PYTHON_VER}/Python-${PYTHON_VER}.tgz -O
echo "${PYTHON_MD5} Python-${PYTHON_VER}.tgz" > python.md5
md5sum -c python.md5
tar -zxf Python-${PYTHON_VER}.tgz

# build python and install into build dir
cd Python-${PYTHON_VER}
BUILD_DIR=$CI_PROJECT_DIR/${PACKAGENAME}-${PYTHON_VER}-${RELEASE}_${ARCH}/usr/local/python/${PYTHON_VER}
mkdir -p ${BUILD_DIR}

if [ "$ARCH" == "armhf" ];
then
    ./configure --prefix=${BUILD_DIR}
else
    ./configure --prefix=${BUILD_DIR} --enable-optimizations
fi

make
make altinstall
${BUILD_DIR}/bin/python$(echo $PYTHON_VER | cut -c1-3) -m ensurepip --upgrade
${BUILD_DIR}/bin/python$(echo $PYTHON_VER | cut -c1-3) -m pip install --upgrade pip
