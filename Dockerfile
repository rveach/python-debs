FROM buildpack-deps:bionic-curl

LABEL maintainer="rveach@gmail.com"

ENV PYTHON_VER=3.7.3
ENV PYTHON_MD5=2ee10f25e3d1b14215d56c3882486fcf
ENV RELEASE=1
ENV ARCH=amd64
ENV PACKAGENAME=altpython
ENV MAINTAINER="Ryan Veach <rveach@gmail.com>"
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get upgrade -y \
    && apt-get install -y \
    build-essential \
    python-dev \
    libreadline-dev \
    libssl-dev \
    libbz2-dev \
    build-essential \
    sqlite3 \
    tk-dev \
    libsqlite3-dev \
    libc6-dev \
    libgdm-dev \
    libncurses-dev \
    libgdbm-dev \
    liblzma-dev \
    curl \
    ca-certificates \
    libffi-dev \
    libgdbm-dev \
    uuid-dev \
    libgdbm-compat-dev \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir /src  && cd /src && mkdir /dist \
    && BUILD_DIR=/build/${PACKAGENAME}-${PYTHON_VER}-${RELEASE}_${ARCH} \
    && curl https://www.python.org/ftp/python/${PYTHON_VER}/Python-${PYTHON_VER}.tgz -O \
    && echo "${PYTHON_MD5} Python-${PYTHON_VER}.tgz" > python.md5 \
    && md5sum -c python.md5 \
    && tar -zxf Python-${PYTHON_VER}.tgz \
    && rm -f Python-${PYTHON_VER}.tgz \
    && mkdir -p ${BUILD_DIR} && mkdir -p ${BUILD_DIR}/DEBIAN \
    && cd /src/Python-${PYTHON_VER} \
    && ./configure --prefix=${BUILD_DIR} --enable-optimizations \
    && make \
    && make altinstall \
    && ${BUILD_DIR}/bin/python$(echo $PYTHON_VER | cut -c1-3) -m pip install --upgrade pip \
    && echo "Package: ${PACKAGENAME}" >> ${BUILD_DIR}/DEBIAN/control \
    && echo "Version: ${PYTHON_VER}-${RELEASE}" >> ${BUILD_DIR}/DEBIAN/control \
    && echo "Section: base" >> ${BUILD_DIR}/DEBIAN/control \
    && echo "Priority: optional" >> ${BUILD_DIR}/DEBIAN/control \
    && echo "Architecture: ${ARCH}" >> ${BUILD_DIR}/DEBIAN/control \
    && echo "Maintainer: ${MAINTAINER}" >> ${BUILD_DIR}/DEBIAN/control \
    && echo "Description: alt install for recent python" >> ${BUILD_DIR}/DEBIAN/control
