#! /bin/bash

set -e

mkdir -p $CI_PROJECT_DIR/${PACKAGENAME}-${PYTHON_VER}-${RELEASE}_${ARCH}/DEBIAN
CONTROL_FILE=$CI_PROJECT_DIR/${PACKAGENAME}-${PYTHON_VER}-${RELEASE}_${ARCH}/DEBIAN/control
COLON=":"
echo "Package${COLON} ${PACKAGENAME}" >> ${CONTROL_FILE}
echo "Version${COLON} ${PYTHON_VER}-${RELEASE}" >> ${CONTROL_FILE}
echo "Section${COLON} base" >> ${CONTROL_FILE}
echo "Priority${COLON} optional" >> ${CONTROL_FILE}
echo "Architecture${COLON} ${ARCH}" >> ${CONTROL_FILE}
echo "Maintainer${COLON} ${MAINTAINER}" >> ${CONTROL_FILE}
echo "Description${COLON} alt install for recent python" >> ${CONTROL_FILE}
dpkg-deb --build ${PACKAGENAME}-${PYTHON_VER}-${RELEASE}_${ARCH} ${PACKAGENAME}-${PYTHON_VER}-${RELEASE}_${ARCH}.deb
rm -rf $CI_PROJECT_DIR/${PACKAGENAME}-${PYTHON_VER}-${RELEASE}_${ARCH}/DEBIAN
