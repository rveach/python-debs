#! /bin/bash

set -e
apt-get update
apt-get upgrade -y
apt-get install -y build-essential libreadline-dev libssl-dev \
    libbz2-dev sqlite3 tk-dev libsqlite3-dev libc6-dev libgdm-dev libgdbm-dev \
    liblzma-dev ca-certificates libffi-dev uuid-dev libgdbm-compat-dev
