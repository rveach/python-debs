#! /bin/bash


DEB_FILENAME="${PACKAGENAME}-${PYTHON_VER}-${RELEASE}_${ARCH}.deb"
BINTRAY_URL="https://api.bintray.com/content/$BINTRAY_ORG/$BINTRAY_REPO/$PACKAGENAME/${RELEASE}/${DEB_FILENAME};deb_architecture=${ARCH}"


ls -lh *.deb
echo "Calling Bintray at ${BINTRAY_URL}"
curl -T ${DEB_FILENAME} -urveach:${BINTRAY_API_KEY} ${BINTRAY_URL}

